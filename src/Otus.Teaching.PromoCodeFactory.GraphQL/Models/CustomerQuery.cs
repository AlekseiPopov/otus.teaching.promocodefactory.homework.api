﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Types;
using HotChocolate.Types.Relay;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.GraphQL.Models
{
    public class CustomerQuery
    {
        private readonly IRepository<Customer> _customerRepository;

        public CustomerQuery(IRepository<Customer> customerRepository)
        {
            _customerRepository = customerRepository;
        }

        [UsePaging(typeof(CustomerType))]
        [UseSorting]
        [UseFiltering]
        public async Task<IEnumerable<Customer>> Customers() => await _customerRepository.GetAllAsync();
    }
}